require 'net/http'
require 'uri'
require 'json'
require 'yaml'

response = Net::HTTP.get(URI.parse('https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/team.yml'))

puts JSON.pretty_generate(YAML.load(response))
